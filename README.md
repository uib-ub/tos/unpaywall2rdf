# Unpaywall to Nquads


A mapping from unpaywall csv dump to Nquads.

## Importer nye data

Slå av kjørende instans av graphdb.

Endre på `unpaywall.properties`

Ta inn ny download lenke, og gi nytt navn til
`unpaywall.repo.name` og `unpaywall.out` i /opt/graphdb/unpaywall2rdf
Dersom en ønsker å gjenbruke navnene, må en slette database og fil (inkludert gz) som ligger ifra før.
** Manuelt endre navn i unpaywall-config.ttl til ... rep:repositoryID "unpaywall-2020-04" ;
 **
Kjør 
```
screen -S importUPW #(navngir screen for å attache igjen)
sudo su gdb
cd /opt/graphdb/unpaywall2rdf
ant -logfile upw-import.log 
```
For å detache kan en `ctrl-a`  så `d`

om en ønsker å se progress i screen kan en så skrive screen -r importUPW

## OOADOI data model

See http://data.unpaywall.org/data-format and https://docs.google.com/document/d/1Whfe26oyjTedeW1GGWkq3NADgDbL2R2eXqrJCWS8vcc/edit
data-format is not entirely up to date.

## Our datamodel
Namespaces - 
```
        http://unpaywall.ub.uib.no/
bibo    http://purl.org/ontology/bibo/
dct     http://purl.org/dc/terms/
ecrm    http://erlangen-crm.org/current/
mmdr    http://purl.org/momayo/mmdr/ 
mmo     http://purl.org/momayo/mmo/
rdfs    http://www.w3.org/2000/01/rdf-schema#
upw     http://unpaywall.ub.uib.no/ontology/
vivo    http://vivoweb.org/ontology/core#
 
```

```
#begin document
<doi-uri>
    a upw:ClosedAccess | upw:OpenAccess | upw:UndefinedAccess ; 
    upw:isOA true | false ; #fjerne?
    ecrm:P2_has_type <http://example.org/id/uuid/1> ; # uuid generated for each type.
    dct:isPartOf <http://example.org/id/uuid/2> ; # uuid generated for each unique issn, and fallback uuid from lower-case(title) if no issn.
    dct:publisher <http//example.org/id/uuid/3> ; # uuid generated for each publisher-name.    
    dct:created "2017"^^xsd:gYear ; #where value is castable as gYear.
 
#    upw:hostType "Repository." ;
    upw:quality "1"xsd:integer; #sjekk
    mmdr:hasURI "uri-to-pdf..."xsd:anyURI ;
    rdfs:label "Title" ;
    dct:modified "2017-09-31"xsd:date ;
    bibo:doi "doi" ;
    upw:location <uuid-location> 

#Begin Location:
                <uuid-location> upw:hostType "host type";
                upw:hasURI <url> ;
                upw:quality "1"^^xsd:integer  #1 eller 2              
                rdf:type upw:Location;
                upw:isBest boolean;
                upw:evidence "evidence";
                upw:license "license" ; # dct:license går til LicenseObject skal en opprette slike?
                upw:version "version".

#begin Type
<http://example.org/id/uuid/1>
    a ecrm:E55_Type ; 
    rdfs:label "Article" .

#Begin Publisher
<http://example.org/id/uuid/2>
    a vivo:Publisher;
    rdfs:label "Springer" . 

#Begin Journal
<http://example.org/id/uuid/3>
    a bibo:Journal;
    upw:isOA true|false ;   
    rdfs:label "Name of Journal." ;
    bibo:issn "issn".       
#Only label if no issn present.
```   



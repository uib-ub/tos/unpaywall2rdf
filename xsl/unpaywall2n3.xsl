<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl [
    <!ENTITY doi "http://dx.doi.org/">
    <!ENTITY bibo "http://purl.org/ontology/bibo/">
    <!ENTITY dct "http://purl.org/dc/terms/">
    <!ENTITY ecrm "http://erlangen-crm.org/current/">
    <!ENTITY mmdr "http://purl.org/momayo/mmdr/"> 
    <!ENTITY mmo "http://purl.org/momayo/mmo/">
    <!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#">
    <!ENTITY upw "http://unpaywall.ub.uib.no/ontology/">
    <!ENTITY vivo "http://vivoweb.org/ontology/core#">
    <!ENTITY upw "http://example.org/ontology/">
    <!ENTITY base "http://unpaywall.ub.uib.no/id/">
    <!ENTITY rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#">
    <!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#">  
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:err="http://www.w3.org/2005/xqt-errors"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:array="http://www.w3.org/2005/xpath-functions/array"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:flub="http://data.ub.uib.no/ns/function-library"    
    exclude-result-prefixes="xs math"
    version="3.0">
     <xsl:param name="oadoi-path" select="'file:/E:/full_dois_2018-03-29T113154.jsonl/full_dois_2018-03-29T113154.jsonl'"/>
    <!--<xsl:param name="oadoi-path" select="'/home/oyvind/repos/systemer/unpaywall2rdf/oa_status_by_doi.csv/data'"/>-->    
    <xsl:import href="lib/functions/nq.xsl"/>
    <xsl:import href="lib/functions/uuid.xsl"/>    
   
   <xsl:output method="text"/>
   
   <xsl:variable name="newline">
       <xsl:text>&#x000A;</xsl:text>
   </xsl:variable>
    <xsl:template match="/">        
         <!-- properties used more than once are added to variables for consistency and ease of change-->
        <xsl:variable name="isOA-uri" select="xs:anyURI('&upw;isOA')"/>
        <xsl:variable name="rdf-type-uri" select="xs:anyURI('&rdf;type')" as="xs:anyURI"/>
        <xsl:variable name="rdfs-label-uri" select="xs:anyURI('&rdfs;label')" as="xs:anyURI"/>
        
        <xsl:iterate select="unparsed-text-lines($oadoi-path)">            
            <xsl:param name="publishers" as="map(xs:string,xs:anyURI)?">
                <xsl:map/>
            </xsl:param>
            <xsl:param name="issns" as="map(xs:string,xs:anyURI)?">
                <xsl:map/>
            </xsl:param>
            <xsl:param name="types" as="map(xs:string,xs:anyURI)?">
                <xsl:map/>
            </xsl:param>
            <xsl:on-completion>
                <xsl:message select="'done!'"/>
            </xsl:on-completion>
            <xsl:variable name="position" as="xs:integer" select="position()"/>            
            <xsl:variable name="json-record" as="map(*)?">
                <xsl:try select="parse-json(.)">                    
                    <xsl:catch>                        
                        <xsl:choose>
                            <xsl:when test="matches(.,'^\\N$')">
                                <xsl:message select="'empty \N'"/>
                                <xsl:sequence select="()"/>
                            </xsl:when>
                            <xsl:otherwise><xsl:message select="$position,$err:code,$err:description,."/></xsl:otherwise>
                        </xsl:choose>
                    </xsl:catch>                        
                </xsl:try>
                
            </xsl:variable>
            
            <xsl:if test="not(empty($json-record))">
                <xsl:if test="$position mod 1000000 = 0">
                    <xsl:message select="$position"/>
                </xsl:if>
                <xsl:variable name="doi" as="xs:string" expand-text="1"
                    >{map:get($json-record,'doi')}</xsl:variable>
                <xsl:variable name="doi-uri" as="xs:string">
                    <xsl:text expand-text="1">&doi;{encode-for-uri($doi)}</xsl:text>
                </xsl:variable>
                <xsl:variable name="journal-name" select="map:get($json-record, 'journal_name')" as="xs:string?"/>
                <xsl:variable name="is-oa" select="map:get($json-record, 'is_oa')" as="xs:boolean?"/>
                <xsl:variable name="doc-type-uri"
                    select="
                        if ($is-oa)
                        then
                            xs:anyURI('&upw;OpenAccess')
                        else
                            if ($is-oa = false()) then
                                xs:anyURI('&upw;ClosedAccess')
                            else
                                xs:anyURI('&upw;UndefinedAccess')"
                    as="xs:anyURI"/>
                <xsl:variable name="journal-issns"
                    select="distinct-values(tokenize(map:get($json-record, 'journal_issns'), ',')[. != '0000-0000'])"
                    as="xs:string*"/>
                <xsl:variable name="add-journal" as="xs:boolean"
                    select="
                        (map:size($issns)=0 and exists($journal-issns[string(.)]))  or
                        (some $x in $journal-issns[string(.)]
                            satisfies not(map:contains($issns, $x)))"/>
                
                
                <xsl:variable name="publisher" select="map:get($json-record, 'publisher')"
                    as="xs:string?"/>
                <xsl:variable name="add-publisher" as="xs:boolean"
                    select="
                        string($publisher) and not(map:contains($publishers, $publisher))
                        "/>
                <xsl:variable name="type" as="xs:string?" select="map:get($json-record, 'genre')"/>
                <xsl:variable name="add-type" as="xs:boolean"
                    select="
                        string-length($type) &gt; 0 and not(map:contains($types, $type))
                        "/>
                <!-- update journal uris with get-->
                <xsl:variable name="journal-map" as="map(xs:string,xs:anyURI)">
                    <xsl:map>
                    <xsl:for-each select="$journal-issns">                      
                        <xsl:map-entry select="if (map:contains($issns,.)) 
                            then map:get($issns, .) 
                            else xs:anyURI(concat('&base;', flub:uuid-from-string(upper-case(.))))" key="."/>
                    </xsl:for-each>
                        <xsl:if test="count($journal-issns)=0 and $journal-name">
                            <xsl:map-entry key="$journal-name" select="xs:anyURI(concat('&base;', flub:uuid-from-string(lower-case($journal-name))))"/>
                        </xsl:if>
                    </xsl:map>
                </xsl:variable>
                
                <xsl:variable name="publisher-uri" as="xs:anyURI?"
                    select="
                        if ($add-publisher)
                        then
                            xs:anyURI(concat('&base;', flub:uuid-from-string($publisher)))
                        else
                            if (empty($publisher)) then
                                ()
                            else
                                map:get($publishers, $publisher)"/>
                <xsl:variable name="type-uri"
                    select="
                        if ($add-type)
                        then
                            xs:anyURI(concat('&base;', flub:uuid-from-string($type)))
                        else
                            if (empty($type)) then
                                ()
                            else
                                map:get($types, $type)"
                    as="xs:anyURI?"/>
                <xsl:variable name="created-year" as="map(xs:string,xs:string)?"
                    select="flub:xsd-literal(string(map:get($json-record, 'year'))[. castable as xs:gYear], 'gYear')"/>
                <xsl:variable name="best-oa-location"
                    select="map:get($json-record, 'best_oa_location')"/>
                <xsl:variable name="oa-locations" select="map:get($json-record, 'oa_locations')"/>
                <xsl:variable name="locations-size" select="array:size($oa-locations)"
                    as="xs:integer"/>
                
                <!-- begin writing triples-->
                <xsl:value-of
                    select="
                        flub:nqobject(
                        $doi-uri,
                        $rdf-type-uri,
                        $doc-type-uri
                        )"/>

                <xsl:value-of
                    select="
                        flub:nqliteral($doi-uri,
                        $isOA-uri,
                        flub:boolean-literal($is-oa))"/>
                <xsl:for-each select="$journal-issns">
                        <xsl:value-of
                    select="
                        flub:nqliteral($doi-uri,
                        xs:anyURI('&upw;issn'),
                        flub:string(.))"/>

                </xsl:for-each>
                <xsl:value-of
                    select="
                        flub:nqobject($doi-uri,
                        xs:anyURI('&ecrm;P2_has_type'),
                        $type-uri)"/>
                
                <xsl:for-each select="map:keys($journal-map)">
                <xsl:value-of
                    select="
                        flub:nqobject($doi-uri,
                        xs:anyURI('&dct;isPartOf'),
                        map:get($journal-map,.))"/>
                </xsl:for-each>                

                <xsl:value-of
                    select="
                        flub:nqobject($doi-uri,
                        xs:anyURI('&dct;publisher'),
                        $publisher-uri)"/>

                <xsl:value-of
                    select="
                        flub:nqliteral($doi-uri,
                        xs:anyURI('&dct;created')
                        , $created-year)"/>
              
                <xsl:value-of
                    select="
                        flub:nqliteral($doi-uri,
                        xs:anyURI('&upw;quality'),
                        flub:xsd-literal(string(map:get($json-record, 'data_standard')),
                        'integer'))"/>

                <xsl:value-of
                    select="
                        if (not(empty($best-oa-location))) then
                            flub:nqliteral($doi-uri,
                            xs:anyURI('&upw;hostType'),
                            flub:string(map:get($best-oa-location, 'host_type'))
                            )
                        else
                            ''"/>

                <xsl:value-of
                    select="
                        if (not(empty($best-oa-location))) then
                            flub:nqliteral($doi-uri,
                            xs:anyURI('&mmdr;hasURI'),
                            flub:xsd-literal(map:get($best-oa-location, 'url'),
                            'anyURI')
                            )
                        else
                            ''"/>

                <xsl:value-of
                    select="
                        flub:nqliteral($doi-uri,
                        $rdfs-label-uri,
                        flub:string(map:get($json-record, 'title'))
                        )"/>

                <xsl:value-of
                    select="
                        flub:nqliteral($doi-uri,
                        xs:anyURI('&bibo;doi'),
                        flub:string($doi)
                        )"/>

                <xsl:value-of
                    select="
                        flub:nqliteral($doi-uri,
                        xs:anyURI('&dct;modified'),
                        flub:xsd-literal(
                        substring(map:get($json-record, 'updated'), 1, 10)[. castable as xs:date],
                        'date'))"/>
               
                <xsl:if test="$add-type">
                    <xsl:value-of
                        select="
                            flub:nqliteral($type-uri,
                            $rdfs-label-uri,
                            flub:string($type))"/>
                    <xsl:value-of
                        select="
                            flub:nqobject($type-uri,
                            $rdf-type-uri,
                            xs:anyURI('&ecrm;E55_Type')
                            )"
                    />
                </xsl:if>
                
                <xsl:if test="$add-publisher">
                    <xsl:value-of
                        select="
                            flub:nqliteral($publisher-uri,
                            $rdfs-label-uri,
                            flub:string($publisher))"/>
                    <xsl:value-of
                        select="
                            flub:nqobject($publisher-uri,
                            $rdf-type-uri,
                            xs:anyURI('&vivo;Publisher')
                            )"
                    />
                </xsl:if>
                
                <xsl:if test="$locations-size > 0">
                    <xsl:for-each select="1 to $locations-size">
                        <xsl:variable name="location" select="array:get($oa-locations, .)"/>
                        <xsl:variable name="locations-url"
                            select="xs:anyURI(concat('&base;', flub:uuid-from-string(map:get($location, 'url'))))"/>

                        <xsl:value-of
                            select="
                                flub:nqobject($doi-uri,
                                xs:anyURI('&upw;location'),
                                $locations-url)"/>

                        <xsl:value-of
                            select="
                                flub:nqliteral($locations-url,
                                xs:anyURI('&upw;quality'),
                                flub:xsd-literal(string(map:get($location, 'data_standard')),
                                'integer'))"/>

                        <xsl:value-of
                            select="
                                flub:nqliteral($locations-url,
                                xs:anyURI('&upw;hostType'),
                                flub:string(map:get($location, 'host_type'))
                                )"/>

                        <xsl:value-of
                            select="
                                flub:nqliteral($locations-url,
                                xs:anyURI('&mmdr;hasURI'),
                                flub:xsd-literal(map:get($location, 'url')
                                , 'anyURI'))"/>

                        <xsl:value-of
                            select="
                                flub:nqobject($locations-url,
                                $rdf-type-uri,
                                xs:anyURI('&upw;Location')
                                )"/>
                        
                        <xsl:value-of select="flub:nqliteral($locations-url,
                            xs:anyURI('&upw;version'),
                            flub:string(map:get($location,'version')))"/>
                        
                        <xsl:value-of
                            select="
                                flub:nqliteral($locations-url,
                                xs:anyURI('&upw;isBest'),
                                flub:boolean-literal(map:get($location, 'is_best')))"/>

                        <xsl:value-of
                            select="
                                flub:nqliteral($locations-url,
                                xs:anyURI('&upw;evidence'),
                                flub:string(map:get($location, 'evidence')))"/>

                        <xsl:value-of
                            select="
                                flub:nqliteral($locations-url,
                                xs:anyURI('&upw;license'),
                                flub:string(map:get($location, 'license')))"/>

                        <xsl:value-of
                            select="
                                flub:nqliteral($locations-url,
                                xs:anyURI('&upw;version'),
                                flub:string(map:get($location, 'version')))"/>

                    </xsl:for-each>
                </xsl:if>
                
                <xsl:value-of select="flub:nqliteral(for $x in map:keys($journal-map) return map:get($journal-map,$x)
                    ,$rdfs-label-uri,
                    flub:string($journal-name))"/>
                
                <xsl:if test="$add-journal">
                    <xsl:for-each select="map:keys($journal-map)">
                        
                        <xsl:variable name="journal-uri" select="map:get($journal-map,.)" as="xs:anyURI"/>
                        <xsl:if test="not(map:contains($issns,.))">
                            <xsl:value-of
                                select="
                                flub:nqobject($journal-uri,
                                $rdf-type-uri,
                                xs:anyURI('&bibo;Journal')
                                )"/>                            
                            <xsl:value-of
                                select="
                                flub:nqliteral($journal-uri,
                                $isOA-uri,
                                flub:boolean-literal(map:get($json-record, 'journal_is_oa')))"/>
                            
                           
                                <xsl:if test="(map:size($issns)=0 or not(map:contains($issns, .))) and exists($journal-issns) ">
                                    <xsl:value-of
                                        select="
                                        flub:nqliteral($journal-uri,
                                        xs:anyURI('&bibo;issn'),
                                        flub:string(.))"
                                    />
                                </xsl:if>
                            
                        </xsl:if>
                    </xsl:for-each>
                  
                </xsl:if>
                <xsl:next-iteration>
                    <xsl:with-param name="publishers"
                        select="
                            if (not($add-publisher))
                            then
                                $publishers
                            else
                                map:put($publishers, $publisher, $publisher-uri)"/>
                    <xsl:with-param name="types"
                        select="
                            if (not($add-type))
                            then
                                $types
                            else
                                map:put($types, $type, $type-uri)"/>
                    <xsl:with-param name="issns"
                        select="
                            if (not($add-journal))
                            then
                                $issns
                            else
                                map:merge(($issns,$journal-map))"
                    />
                </xsl:next-iteration>
            </xsl:if>
        </xsl:iterate>        
    </xsl:template>  
</xsl:stylesheet>
